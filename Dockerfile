FROM node:16 as builder

WORKDIR /app

COPY . .

RUN npm ci

RUN npm run pkg:cjs

FROM ubuntu:latest

WORKDIR /eapp

COPY --from=builder /app/dist/expressjs /eapp/expressjs

RUN ls -la

EXPOSE 3009

ENTRYPOINT ["/eapp/expressjs"]


